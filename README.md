## Background

This repository host the files needed to reproduce the results in the paper

Bhatia, N, Bozorg, B, Larsson, A, Ohno, C, Jönsson, H, Heisler, MG (2016)
Auxin Acts through MONOPTEROS to Regulate Plant Cell Polarity and Pattern Phyllotaxis
Curr Biol 26, 3202-3208, https://doi.org/10.1016/j.cub.2016.09.044

## Files

In the <tt>modelFiles</tt> folder two sub-folders for the <tt>auxinModel</tt> and
<tt>mechanicalModel</tt> are provided where the files for running the simulations 
are provided. Readme files within the sub-folders provide further information on how to 
produce and plot the simulation results.

## Software

Simulations have been done using the [tissue](https://gitlab.com/slcu/teamhj/tissue) software 
available via the Sainsbury Laboratory gitlab repository, and instructions for downloading, compiling
and running simulations in general are available in the Documentation within the tissue repository.
A short description is also given in the REadme files for running simulations in this repository.

## Contact

Main contact is henrik.jonsson@slcu.cam.ac.uk.